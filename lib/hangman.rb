class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(hash)
    @guesser = hash[:guesser]
    @referee = hash[:referee]
    @board = hash[:board]
  end

  def guesser
    @guesser
  end

  def referee
    @referee
  end

  def board
    @board
  end

  def setup
    length = @referee.pick_secret_word
    @board = "_"*length
    @guesser.register_secret_length(length)

  end

  def take_turn
    guess = @guesser.guess
    idx = @referee.check_guess(guess)
    update_board(idx, guess)
    @guesser.handle_response
  end

  def update_board(idx, guess)
    idx.each {|i| @board[i] = guess}
  end
end


class HumanPlayer
  def initialize(dictionary)
    @dictionary = dictionary
  end


  def guess
    puts "enter a guess: "
    guess = gets.chomp
  end


end

class ComputerPlayer
  attr_reader :dictionary
  attr_accessor :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = @dictionary
    @guessed = []
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    indexes = []
    @secret_word.split("").each_with_index do |let, idx|
      indexes << idx if let ==letter
    end
    @guessed << letter
    indexes
  end

  def register_secret_length(length)
    puts "the secret word is #{length} letters long"
    @candidate_words= self.candidate_words.select {|word| word.length == length}
  end

  def guess(board)
    guess = self.frequency(board)
    guess.sort_by {|k, v| v}[-1][0]

  end


  def candidate_words
    @candidate_words
  end

  def frequency(board)
    freq = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |letter, idx|
        freq[word[idx]] +=1 if letter.nil?
      end
    end
    freq
  end

=begin
    freq = Hash.new(0)
    self.candidate_words.join.split("").each do |x|
      freq[x] +=1
    end
    letter = nil
    value = 0
    freq.each do |k, v|
      if v > value && !@guessed.include?(k)
        letter = k
        value = v
      end
    end
    freq
  end
=end


  def handle_response(letter, index)
    self.candidate_words.each do |word|
      index.each do |idx|
        if word[idx]!=letter
          self.candidate_words.delete(word)
        end
      end
      self.candidate_words.delete(word) if word.count(letter) > index.length
    end
  end

end
